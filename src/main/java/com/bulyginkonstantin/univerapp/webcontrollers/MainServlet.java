package com.bulyginkonstantin.univerapp.webcontrollers;

import com.bulyginkonstantin.univerapp.dao.DaoUniversity;
import com.bulyginkonstantin.univerapp.entity.Faculty;
import com.bulyginkonstantin.univerapp.entity.Teacher;
import com.bulyginkonstantin.univerapp.entity.Univer;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * Главный сервлет, обрабатывает все запросы
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/"})
public class MainServlet extends HttpServlet {

    // объект для выполнения запросов в базу данных
    private DaoUniversity daoUniversity;

    // объект для оплучения доступа к ресурсам
    // аннотация @Resource(name = "jdbc/univerPool") используется для 
    // внедрения ресурсов настроеных на сервере GlassFish
    @Resource(name = "jdbc/univerPool")
    private DataSource dataSource;

    // инициализирует объект DaoUniversity используя объект DataSource перед началом работы сервлета
    @Override
    public void init() throws ServletException {
        super.init();

        try {
            // инициализирует объект DaoUniversity используя объект DataSource
            daoUniversity = new DaoUniversity(dataSource);
        } catch (Exception exc) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, exc);
        }
    }

    // обрабатывает GET запросы 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // получаем эндпойнт
        String action = request.getServletPath();

        // проверяем эндпойнт и на его основе выполняем действия
        switch (action) {

            case "/" ->
                // показываем основную страницу
                showMainPage(request, response);

            case "/admin" ->
                // показываем страницу администратора
                showAdminPage(request, response);

            case "/faculties" ->
                // показываем страницу с факультетами
                showFaculties(request, response);

            case "/teachers" ->
                // показываем страницу с преподавателями
                showTeachers(request, response);

            case "/contact" ->
                // показываем страницу с контактами
                showContacts(request, response);

            case "/search" -> {

                // если метод GET показываем страницу поиска
                if (request.getMethod().equals("GET")) {
                    showSearchForm(request, response);
                }
                // если метод POST выполняем поиск
                if (request.getMethod().equals("POST")) {
                    searchData(request, response);
                }
            }

            case "/editListTeacher" ->
                // показываем страницу с преподавателями для редактирования
                showTeacherListToEdit(request, response);

            case "/editTeacher" -> {

                // если метод GET то показываем страницу с данными преподавателя для редактирования
                if (request.getMethod().equals("GET")) {
                    showFormToEditTeacher(request, response);
                }
                // если метод POST то обновляем данные преподавателя в базе данных
                if (request.getMethod().equals("POST")) {
                    updateTeacherInDb(request, response);
                }
            }

            case "/deleteTeacher" ->
                // удалем преподавателя
                deleteTeacher(request, response);

            case "/addTeacher" -> {

                // если метод GET то показываем страницу с формой для добавления преподавателя
                if (request.getMethod().equals("GET")) {
                    showFormToAddTeacher(request, response);
                }
                // если метод POST то добавляем данные преподавателя в базу данных
                if (request.getMethod().equals("POST")) {
                    addTeacherToDb(request, response);
                }
            }

            case "/editListFaculty" ->
                // показываем страницу с факультетами для редактирования
                showFacultyListToEdit(request, response);

            case "/editFaculty" -> {

                // если метод GET то показываем страницу с данными факультета для редактирования
                if (request.getMethod().equals("GET")) {
                    showFormToEditFaculty(request, response);
                }

                // если метод POST то обновляем данные факультета в базе данных
                if (request.getMethod().equals("POST")) {
                    updateFacultyInDb(request, response);
                }
            }

            case "/deleteFaculty" ->
                // удалем факультет
                deleteFaculty(request, response);

            case "/addFaculty" -> {

                // если метод GET то показываем страницу с формой для добавления факультета
                if (request.getMethod().equals("GET")) {
                    showFormToAddFaculty(request, response);
                }

                // если метод POST то добавляем данные факультета в базу данных
                if (request.getMethod().equals("POST")) {
                    addFacultyToDb(request, response);
                }
            }
        }
    }

    // обрабатывает POST запросы
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    /**
     * Запрашивает из базы данных всех преподавателей и направляет на страницу
     * администрирования
     */
    private void showTeacherListToEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получаем список преподавателей
            List<Teacher> teachers = daoUniversity.getTeachers();
            // добавляем список преподавателей к запросу
            request.setAttribute("TEACHER_LIST", teachers);
            // перенправляем на страницу univer-teacher-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-teacher-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Направляет на страницу поиска
     */
    private void showSearchForm(HttpServletRequest request, HttpServletResponse response) {
        try {
            // перенправляем на страницу univer-search-form.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-search-form.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Направляет на страницу контактов
     */
    private void showContacts(HttpServletRequest request, HttpServletResponse response) {
        try {
            // перенправляем на страницу univer-contact.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-contact.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Направляет на страницу администратора
     */
    private void showAdminPage(HttpServletRequest request, HttpServletResponse response) {
        try {
            // перенправляем на страницу univer-admin.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-admin.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Направляет на главную страницу
     */
    private void showMainPage(HttpServletRequest request, HttpServletResponse response) {
        try {
            // перенправляем на страницу index.jsp
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Производит поиск факультета по названию, направляет на страницу с
     * результатами поиска, записывает результаты в XML файл
     */
    private void searchData(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            // получение парамметров запроса
            String facultyRequest = request.getParameter("facultyRequest");

            // если запрос не равен null производится поиск факультета
            if (facultyRequest != null) {
                Faculty faculty = daoUniversity.findFacultyByName(facultyRequest);

                // если факультет равен null производится направление на страницу результатов
                if (faculty == null) {
                    // установка аттрибута
                    request.setAttribute("FACULTY", null);
                    // направление на страницу результатов
                    request.getRequestDispatcher("/WEB-INF/views/univer-request-result.jsp").forward(request, response);
                    // иначе 
                } else {
                    // установка аттрибута для факультета
                    request.setAttribute("FACULTY", faculty);
                    // поиск преподавателей в базе данных
                    List<Teacher> teachers = daoUniversity.findTeachersByFacultyId(faculty.getFacultyId());
                    // установка аттрибута для преподавателей
                    request.setAttribute("TEACHER_LIST", teachers);
                    // запись результатов в xml файл
                    writeToXmlFacultyTeachers(faculty, teachers);
                    // перенправляем на страницу univer-request-result.jsp
                    request.getRequestDispatcher("/WEB-INF/views/univer-request-result.jsp").forward(request, response);
                }
            }
        } catch (ServletException | IOException | NumberFormatException | ParserConfigurationException | TransformerException | XMLStreamException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            // перенправляем на страницу univer-request-result.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-request-result.jsp").forward(request, response);
        }
    }

    /**
     * Запрашивает из базы данных все факультеты и направляет на страницу
     * отображения факультетов
     */
    private void showFaculties(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получаем список факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // устанавливаем аттрибут для списка факультетов
            request.setAttribute("FACULTY_LIST", faculties);
            // перенправляем на страницу univer-faculty-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-faculty-list.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Запрашивает из базы данных всех преподавателей и направляет на страницу
     * отображения преподавателей
     */
    private void showTeachers(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получаем список преподавателей
            List<Teacher> teachers = daoUniversity.getTeachers();
            // устанавливаем аттрибут для списка преподавателей
            request.setAttribute("TEACHER_LIST", teachers);
            // перенправляем на страницу univer-teacher-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-teacher-list.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Записывает в xml файл результаты запросы поиска
     *
     */
    private void writeToXmlFacultyTeachers(Faculty faculty, List<Teacher> teachers) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, XMLStreamException {

        // получение пути для сохранения xml файла с результатами поиска
        Path xmlResultPath = Paths.get("/Users/konst/Downloads/result_faculties.xml");

        // создаем объект OutputStream для записи файла
        try (OutputStream out = Files.newOutputStream(xmlResultPath)) {

            // создаем объект XMLOutputFactory для записи xml в файла
            XMLOutputFactory output = XMLOutputFactory.newInstance();

            // создаем объект XMLStreamWriter для записи xml в файла
            XMLStreamWriter writer = output.createXMLStreamWriter(out);

            // записываем начало xml документа
            writer.writeStartDocument("utf-8", "1.0");

            // записываем начало элемента Univer
            writer.writeStartElement("Univer");
            // записываем аттрибут элемента Univer, пространство имен для схемы Univer
            writer.writeAttribute("xmlns", "http://www.gooduniver.com/Univer");
            // записываем аттрибут элемента Univer, пространство имен для схемы XMLSchema-instance
            writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            // записываем аттрибут элемента Univer, расположение схемы univer.xsd
            writer.writeAttribute("xsi:schemaLocation", "http://www.gooduniver.com/Univer univer.xsd");

            // записываем начало элемента Faculty
            writer.writeStartElement("faculty");
            // записываем начало элемента faculty_id
            writer.writeStartElement("faculty_id");
            // записываем данные для faculty_id
            writer.writeCharacters(faculty.getFacultyId().toString());
            // записываем конец элемента faculty_id
            writer.writeEndElement();

            // записываем начало элемента faculty_name
            writer.writeStartElement("faculty_name");
            // записываем данные для faculty_name
            writer.writeCharacters(faculty.getFacultyName());
            // записываем конец элемента faculty_name
            writer.writeEndElement();

            // записываем начало элемента univer_id
            writer.writeStartElement("univer_id");
            // записываем данные для univer_id
            writer.writeCharacters(faculty.getUniverId().getUniverId().toString());
            // записываем конец элемента univer_id
            writer.writeEndElement();

            // проход в цикле по списку преподавателей
            for (Teacher teacher : teachers) {

                // записываем начало элемента Teacher
                writer.writeStartElement("teacher");

                // записываем начало элемента teacher_id
                writer.writeStartElement("teacher_id");
                // записываем данные для teacher_id
                writer.writeCharacters(teacher.getTeacherId().toString());
                // записываем конец элемента teacher_id
                writer.writeEndElement();

                // записываем начало элемента first_name
                writer.writeStartElement("first_name");
                // записываем данные для first_name
                writer.writeCharacters(teacher.getFirstName());
                // записываем конец элемента first_name
                writer.writeEndElement();

                // записываем начало элемента last_name
                writer.writeStartElement("last_name");
                // записываем данные для last_name
                writer.writeCharacters(teacher.getLastName());
                // записываем конец элемента last_name
                writer.writeEndElement();

                // записываем начало элемента email
                writer.writeStartElement("email");
                // записываем данные для email
                writer.writeCharacters(teacher.getEmail());
                // записываем конец элемента email
                writer.writeEndElement();

                // записываем начало элемента faculty_id
                writer.writeStartElement("faculty_id");
                // записываем данные для faculty_id
                writer.writeCharacters(teacher.getFaculty().getFacultyId().toString());
                // записываем конец элемента faculty_id
                writer.writeEndElement();

                // записываем конец элемента Teacher
                writer.writeEndElement();
            }

            // записываем конец элемента Faculty
            writer.writeEndElement();

            // записываем конец элемента Univer
            writer.writeEndElement();

            // записываем конец документа
            writer.writeEndDocument();

            // завершаем запись 
            writer.flush();

            // закрываем объект записи
            writer.close();

        } catch (IOException | XMLStreamException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Отображает форму с данными по выбранному преподавателю для редактирования
     *
     */
    private void showFormToEditTeacher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получение парамметров запроса, id преподавателя
            Integer teacherId = Integer.valueOf(request.getParameter("id"));
            // поиск преподавателя
            Teacher teacher = daoUniversity.findTeacherById(teacherId);
            // установка аттрибута для teacher
            request.setAttribute("teacher", teacher);
            // поиск факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу с формой редактирования преподавателя
            request.getRequestDispatcher("/WEB-INF/views/teacher-edit-form.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Обновляет преподавателя в базе данных
     *
     */
    private void updateTeacherInDb(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получение парамметров запроса, id преподавателя
            Integer teacherId = Integer.valueOf(request.getParameter("teacherId"));
            // получение парамметров запроса, firstName преподавателя
            String firstName = request.getParameter("firstName");
            // получение парамметров запроса, lastName преподавателя
            String lastName = request.getParameter("lastName");
            // получение парамметров запроса, email преподавателя
            String email = request.getParameter("email");
            // получение парамметров запроса, facultyId преподавателя
            Integer facultyId = Integer.valueOf(request.getParameter("facultyId"));
            // получение факультета по id
            Faculty faculty = daoUniversity.findFacultyById(facultyId);
            // создание объекта teacher
            Teacher teacher = new Teacher(teacherId, firstName, lastName, email, faculty);
            // обновление записи в базе данных
            daoUniversity.updateTeacher(teacher);
            // получение списка преподавателей
            List<Teacher> teachers = daoUniversity.getTeachers();
            // установка аттрибута для teachers
            request.setAttribute("TEACHER_LIST", teachers);
            // перенаправляет на страницу для администрирования
            request.getRequestDispatcher("/WEB-INF/views/univer-teacher-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            // перенаправляет на страницу teacher-edit-form.jsp
            request.getRequestDispatcher("/WEB-INF/views/teacher-edit-form.jsp").forward(request, response);
        }
    }

    /**
     * Удаляет преподавателя в базе данных
     *
     */
    private void deleteTeacher(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            // получение парамметров запроса
            Integer teacherId = Integer.valueOf(request.getParameter("id"));
            // удаление преподавателя по id
            daoUniversity.deleteTeacherById(teacherId);
            // получение списка преподавателей            
            List<Teacher> teachers = daoUniversity.getTeachers();
            // установка аттрибута для teachers
            request.setAttribute("TEACHER_LIST", teachers);
            // перенаправляет на страницу univer-teacher-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-teacher-admin-list.jsp").forward(request, response);

        } catch (jakarta.servlet.ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Добавляет преподавателя в базу данных
     *
     */
    private void addTeacherToDb(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получение парамметров запроса firstName
            String firstName = request.getParameter("firstName");
            // получение парамметров запроса lastName
            String lastName = request.getParameter("lastName");
            // получение парамметров запроса email
            String email = request.getParameter("email");
            // получение парамметров запроса facultyId
            Integer facultyId = Integer.valueOf(request.getParameter("facultyId"));
            // получение факультета 
            Faculty faculty = daoUniversity.findFacultyById(facultyId);
            // создание объекта teacher
            Teacher teacher = new Teacher(firstName, lastName, email, faculty);
            // добавление в базу данных преподавателя
            daoUniversity.insertTeacher(teacher);
            // получение списка преподавателей 
            List<Teacher> teachers = daoUniversity.getTeachers();
            // установка аттрибута для teachers
            request.setAttribute("TEACHER_LIST", teachers);
            // перенаправляет на страницу univer-teacher-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-teacher-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            request.getRequestDispatcher("/WEB-INF/views/teacher-add-form.jsp").forward(request, response);
        }
    }

    /**
     * Отображает форму для добавления преподавателя в базу данных
     *
     */
    private void showFormToAddTeacher(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получение списка факультетов  
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу teacher-add-form.jsp
            request.getRequestDispatcher("/WEB-INF/views/teacher-add-form.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Отображает форму со списком факультетов для редактирования
     *
     */
    private void showFacultyListToEdit(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получение списка факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу univer-faculty-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-faculty-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Отображает форму с данными для редактирования факультета
     *
     */
    private void showFormToEditFaculty(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получение парамметров запроса
            Integer facultyId = Integer.valueOf(request.getParameter("id"));
            // получение факультета
            Faculty faculty = daoUniversity.findFacultyById(facultyId);
            // установка аттрибута для faculty
            request.setAttribute("faculty", faculty);
            // перенаправляет на страницу faculty-edit-form.jsp
            request.getRequestDispatcher("/WEB-INF/views/faculty-edit-form.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Обновляет данные факультета в базе данных
     *
     */
    private void updateFacultyInDb(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            // получение парамметров запроса facultyId
            Integer facultyId = Integer.valueOf(request.getParameter("facultyId"));
            // получение парамметров запроса facultyName
            String facultyName = request.getParameter("facultyName");
            // получение факультета
            Faculty faculty = new Faculty(facultyId, facultyName);
            Univer univer = new Univer();
            univer.setUniverId(1);
            faculty.setUniverId(univer);
            // обновление факультета           
            daoUniversity.updateFaculty(faculty);
            // получение списка факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу univer-faculty-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-faculty-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            // перенаправляет на страницу faculty-edit-form
            request.getRequestDispatcher("/WEB-INF/views/faculty-edit-form.jsp").forward(request, response);
        }
    }

    /**
     * Удаляет факультет в базе данных
     *
     */
    private void deleteFaculty(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получение парамметров запроса
            Integer facultyId = Integer.valueOf(request.getParameter("id"));
            // удаление факультета
            daoUniversity.deleteFacultyById(facultyId);
            // получение списка факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу univer-faculty-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-faculty-admin-list.jsp").forward(request, response);

        } catch (jakarta.servlet.ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Отображает форму для добавления факультета в базу данных
     *
     */
    private void showFormToAddFaculty(HttpServletRequest request, HttpServletResponse response) {
        try {
            // получение списка факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу faculty-add-form.jsp
            request.getRequestDispatcher("/WEB-INF/views/faculty-add-form.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Добавляет факультет в базу данных
     *
     */
    private void addFacultyToDb(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // получение парамметров запроса
            String facultyName = request.getParameter("facultyName");
            // создание объекта Faculty
            Faculty faculty = new Faculty(facultyName);
            Univer univer = new Univer();
            univer.setUniverId(1);
            faculty.setUniverId(univer);
            // добавление факультета в базу даннх
            daoUniversity.insertFaculty(faculty);
            // получение списка факультетов
            List<Faculty> faculties = daoUniversity.getAllFaculties();
            // установка аттрибута для faculties
            request.setAttribute("FACULTY_LIST", faculties);
            // перенаправляет на страницу univer-faculty-admin-list.jsp
            request.getRequestDispatcher("/WEB-INF/views/univer-faculty-admin-list.jsp").forward(request, response);
        } catch (ServletException | IOException | NumberFormatException ex) {
            // логирование ошибок
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
            request.getRequestDispatcher("/WEB-INF/views/faculty-add-form.jsp").forward(request, response);
        }
    }

}
