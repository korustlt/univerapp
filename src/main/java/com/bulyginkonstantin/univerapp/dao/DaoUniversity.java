package com.bulyginkonstantin.univerapp.dao;

import com.bulyginkonstantin.univerapp.entity.Faculty;
import com.bulyginkonstantin.univerapp.entity.Teacher;
import com.bulyginkonstantin.univerapp.entity.Univer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 * Класс для работы с базой данных
 */
public class DaoUniversity {

    /**
     * Объект DataSource для работы с подключениями
     */
    private final DataSource dataSource;

    /**
     * Основной конструктор DaoUniversity
     *
     * @param theDataSource
     */
    public DaoUniversity(DataSource theDataSource) {
        // инициализация объекта DataSource
        dataSource = theDataSource;
    }

    /**
     * Производит поиск преподавателей по id факультета
     *
     * @param facultyId
     * @return
     */
    public List<Teacher> findTeachersByFacultyId(Integer facultyId) {

        // объявляем список преподавателей
        List<Teacher> teachers = new ArrayList<>();

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        // объект факультета найденого по id
        Faculty faculty = findFacultyById(facultyId);

        try {
            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "SELECT * FROM teacher WHERE faculty_id = ?";
            // создаем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setInt(1, facultyId);
            // выполняем запрос
            myRs = myStmt.executeQuery();

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {

                // получаем данные из объекта ResultSet
                Integer teacherId = myRs.getInt("teacher_id");
                String firstName = myRs.getString("first_name");
                String lastName = myRs.getString("last_name");
                String email = myRs.getString("email");
                // добавляем объект Teacher в список teachers
                teachers.add(new Teacher(teacherId, firstName, lastName, email, faculty));
            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }

        // возвращаем список преподавателей
        return teachers;
    }

    /**
     * Производит поиск факультета по названию факультета
     *
     * @param facultyRequest
     * @return
     */
    public Faculty findFacultyByName(String facultyRequest) {

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;
        // объявляем объект Faculty
        Faculty faculty = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "SELECT * FROM faculty WHERE faculty_name = ?";
            // создаем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setString(1, facultyRequest);
            // выполняем запрос
            myRs = myStmt.executeQuery();

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {
                // получаем данные из объекта ResultSet
                Integer facultyId = myRs.getInt("faculty_id");
                String facultyName = myRs.getString("faculty_name");
                Integer univerId = myRs.getInt("univer_id");
                Univer univer = new Univer(univerId, "Good univer");
                // инициализируем объект Faculty
                faculty = new Faculty(facultyId, facultyName, univer);
            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }
        // возвращаем объект faculty
        return faculty;
    }

    /**
     * Добавляет факульте в базу данных
     *
     * @param faculty
     */
    public void insertFaculty(Faculty faculty) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "INSERT INTO faculty(faculty_name, univer_id) values(?,?)";
            // создаем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметры в запрос
            myStmt.setString(1, faculty.getFacultyName());
            myStmt.setInt(2, faculty.getUniverId().getUniverId());
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Добавляет преподавателя в базу данных
     *
     * @param teacher
     */
    public void insertTeacher(Teacher teacher) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "INSERT INTO teacher(first_name, last_name, email, faculty_id) values(?,?,?,?)";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметры в запрос
            myStmt.setString(1, teacher.getFirstName());
            myStmt.setString(2, teacher.getLastName());
            myStmt.setString(3, teacher.getEmail());
            myStmt.setInt(4, teacher.getFaculty().getFacultyId());
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Удаляет факультет по id
     *
     * @param facultyId
     */
    public void deleteFacultyById(Integer facultyId) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "DELETE FROM faculty WHERE faculty_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setInt(1, facultyId);
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Удаляет преподавателя по id
     *
     * @param teacherId
     */
    public void deleteTeacherById(Integer teacherId) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "DELETE FROM teacher WHERE teacher_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setInt(1, teacherId);
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Обновляет факультет
     *
     * @param faculty
     */
    public void updateFaculty(Faculty faculty) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "UPDATE faculty SET faculty_name = ? WHERE faculty_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметры в запрос
            myStmt.setString(1, faculty.getFacultyName());
            myStmt.setInt(2, faculty.getFacultyId());
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Обновляет преподавателя
     *
     * @param teacher
     */
    public void updateTeacher(Teacher teacher) {

        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "UPDATE teacher SET first_name = ?, last_name = ?, email = ?, faculty_id = ? WHERE teacher_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметры в запрос
            myStmt.setString(1, teacher.getFirstName());
            myStmt.setString(2, teacher.getLastName());
            myStmt.setString(3, teacher.getEmail());
            myStmt.setInt(4, teacher.getFaculty().getFacultyId());
            myStmt.setInt(5, teacher.getTeacherId());
            // выполняем запрос
            myStmt.executeUpdate();

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt);
        }
    }

    /**
     * Поиск факультета по id
     *
     * @param facultyId
     * @return
     */
    public Faculty findFacultyById(Integer facultyId) {

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;
        // объявляем объект факультета
        Faculty faculty = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "select * from faculty where faculty_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setInt(1, facultyId);
            // выполняем запрос
            myRs = myStmt.executeQuery();

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {

                // получаем данные из объекта ResultSet
                String facultyName = myRs.getString("faculty_name");
                Integer univerId = myRs.getInt("univer_id");
                Univer univer = new Univer(univerId, "Good univer");
                // инициализируем объект Faculty
                faculty = new Faculty(facultyId, facultyName, univer);
            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }
        // возвращаем объект faculty
        return faculty;
    }

    /**
     * Поиск преподавателя по id
     *
     * @param teacherId
     * @return
     */
    public Teacher findTeacherById(Integer teacherId) {

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;
        // объявляем объект Teacher
        Teacher teacher = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "SELECT * FROM teacher WHERE teacher_id = ?";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // передаем параметр в запрос
            myStmt.setInt(1, teacherId);
            // выполняем запрос
            myRs = myStmt.executeQuery();

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {

                // получаем данные из объекта ResultSet
                String firstName = myRs.getString("first_name");
                String lastName = myRs.getString("last_name");
                String email = myRs.getString("email");
                Integer facultyId = myRs.getInt("faculty_id");
                // поиск факультета по id
                Faculty faculty = findFacultyById(facultyId);
                // инициализируем объект Teacher
                teacher = new Teacher(teacherId, firstName, lastName, email, faculty);

            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }
        // возвращаем объект teacher
        return teacher;
    }

    /**
     * Поиск всех факультетов
     *
     * @return
     */
    public List<Faculty> getAllFaculties() {

        // объявляем список факультетов
        List<Faculty> faculties = new ArrayList<>();

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "SELECT * FROM faculty";
            // инициализируем объект PreparedStatement с запросом            
            myStmt = myConn.prepareStatement(query);
            // выполняем запрос
            myRs = myStmt.executeQuery(query);

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {

                // получаем данные из объекта ResultSet
                Integer facultyId = myRs.getInt("faculty_id");
                String facultyName = myRs.getString("faculty_name");
                Integer univerId = myRs.getInt("univer_id");
                Univer univer = new Univer(univerId, "Good univer");
                // добавляем объект Faculty в список faculties
                faculties.add(new Faculty(facultyId, facultyName, univer));
            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }
        // возвращаем объект teacher
        return faculties;
    }

    /**
     * Производит поиск преподавателей
     *
     * @return
     */
    public List<Teacher> getTeachers() {

        // объявляем список преподавателей
        List<Teacher> teachers = new ArrayList<>();
        // объявляем список факультетов
        List<Faculty> faculties = new ArrayList<>();

        // объявляем объект ResultSet
        ResultSet myRs = null;
        // объявляем объект Connection
        Connection myConn = null;
        // объявляем объект PreparedStatement
        PreparedStatement myStmt = null;

        try {

            // получение подключения через объект dataSource
            myConn = dataSource.getConnection();
            // строка запроса в базу данных
            String query = "SELECT * FROM teacher";
            // инициализируем объект PreparedStatement с запросом 
            myStmt = myConn.prepareStatement(query);
            // выполняем запрос
            myRs = myStmt.executeQuery(query);

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {

                // получаем данные из объекта ResultSet
                Integer teacherId = myRs.getInt("teacher_id");
                String firstName = myRs.getString("first_name");
                String lastName = myRs.getString("last_name");
                String email = myRs.getString("email");
                Integer facultyId = myRs.getInt("faculty_id");
                // добавляем объект Teacher в список teachers
                teachers.add(new Teacher(teacherId, firstName, lastName, email, new Faculty(facultyId)));
            }

            // строка запроса в базу данных
            query = "SELECT * FROM faculty";
            // инициализируем объект PreparedStatement с запросом
            myStmt = myConn.prepareStatement(query);
            // выполняем запрос
            myRs = myStmt.executeQuery(query);

            // проходим в цикле по объекту ResultSet
            while (myRs.next()) {
                // получаем данные из объекта ResultSet
                Integer facultyId = myRs.getInt("faculty_id");
                String facultyName = myRs.getString("faculty_name");
                Integer univerId = myRs.getInt("univer_id");
                Univer univer = new Univer(univerId, "Good univer");
                // добавляем объект Faculty в список faculties
                faculties.add(new Faculty(facultyId, facultyName, univer));
            }

            // проходим в цикле по списку преподавателей
            for (Teacher teacher : teachers) {
                // проходим в цикле по списку факультетов
                for (Faculty faculty : faculties) {
                    // если id факультета в объекте Teacher равно id факультета в объекте Faculty
                    // присваиваем имя объекта Faculty имени объекта Faculty в объекте Teacher
                    if (Objects.equals(teacher.getFaculty().getFacultyId(), faculty.getFacultyId())) {
                        teacher.getFaculty().setFacultyName(faculty.getFacultyName());
                        teacher.getFaculty().setUniverId(faculty.getUniverId());
                    }
                }
            }

        } catch (SQLException ex) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // закрываем JDBC объекты
            close(myConn, myStmt, myRs);
        }
        // возвращаем список teachers
        return teachers;
    }

    /**
     * Закрывает подключение Connection и объекты PreparedStatement и ResultSet
     */
    private void close(Connection myConn, PreparedStatement myStmt, ResultSet myRs) {

        try {
            if (myRs != null) {
                myRs.close();
            }

            if (myStmt != null) {
                myStmt.close();
            }

            if (myConn != null) {
                myConn.close();
            }
        } catch (SQLException err) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, err);
        }
    }

    /**
     * Закрывает подключение Connection и объект PreparedStatement
     */
    private void close(Connection myConn, PreparedStatement myStmt) {

        try {

            if (myStmt != null) {
                myStmt.close();
            }

            if (myConn != null) {
                myConn.close();
            }
        } catch (SQLException err) {
            // логирование ошибок
            Logger.getLogger(DaoUniversity.class.getName()).log(Level.SEVERE, null, err);
        }
    }

}
