package com.bulyginkonstantin.univerapp.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * Класс "Teacher" представляет собой таблицу "teacher" в базе данных
 */
@Entity
@Table(name = "teacher", catalog = "univer_db", schema = "")
public class Teacher implements Serializable {

    private static final long serialVersionUID = 2L;

    // поле класса представляющее колонку в таблице базы данных teacher_id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "teacher_id", nullable = false)
    private Integer teacherId;

    // поле класса представляющее колонку в таблице базы данных first_name
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "first_name", nullable = false, length = 45)
    private String firstName;

    // поле класса представляющее колонку в таблице базы данных last_name
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "last_name", nullable = false, length = 45)
    private String lastName;

    // поле класса представляющее колонку в таблице базы данных email
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "email", nullable = false, length = 45)
    private String email;

    // поле класса представляющее колонку в таблице базы данных faculty_id
    @JoinColumn(name = "faculty_id", referencedColumnName = "faculty_id", nullable = false)
    @ManyToOne(optional = false)
    private Faculty faculty;

    // конструкторы
    public Teacher() {
    }

    public Teacher(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Teacher(Integer teacherId, String firstName, String lastName, String email) {
        this.teacherId = teacherId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Teacher(Integer teacherId, String firstName, String lastName, String email, Faculty faculty) {
        this.teacherId = teacherId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.faculty = faculty;
    }

    public Teacher(String firstName, String lastName, String email, Faculty faculty) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.faculty = faculty;
    }

    // геттеры и сеттеры
    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    // строковое представление объекта
    @Override
    public String toString() {
        return "Teacher[ teacherId=" + teacherId + " ]";
    }

}
