package com.bulyginkonstantin.univerapp.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

/**
 * Класс "Faculty" представляет собой таблицу "faculty" в базе данных
 */
@Entity
@Table(name = "faculty", catalog = "univer_db", schema = "")
public class Faculty implements Serializable {

    private static final long serialVersionUID = 1L;

    // поле класса представляющее колонку в таблице базы данных faculty_id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "faculty_id", nullable = false)
    private Integer facultyId;

    // поле класса представляющее колонку в таблице базы данных faculty_name
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "faculty_name", nullable = false, length = 50)
    private String facultyName;

    // поле класса представляющее список преподавателей
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "faculty")
    private Collection<Teacher> teacherCollection;

    // поле класса представляющее колонку в таблице базы данных univer_id
    @JoinColumn(name = "univer_id", referencedColumnName = "univer_id", nullable = false)
    @ManyToOne(optional = false)
    private Univer univerId;

    // конструкторы
    public Faculty() {
    }

    public Faculty(Integer facultyId) {
        this.facultyId = facultyId;
    }

    public Faculty(Integer facultyId, String facultyName) {
        this.facultyId = facultyId;
        this.facultyName = facultyName;
    }

    public Faculty(Integer facultyId, String facultyName, Univer univer) {
        this.facultyId = facultyId;
        this.facultyName = facultyName;
        this.univerId = univer;
    }

    public Faculty(String facultyName) {
        this.facultyName = facultyName;
    }

    // геттеры и сеттеры
    public Integer getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Integer facultyId) {
        this.facultyId = facultyId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Collection<Teacher> getTeacherCollection() {
        return teacherCollection;
    }

    public void setTeacherCollection(Collection<Teacher> teacherCollection) {
        this.teacherCollection = teacherCollection;
    }

    public Univer getUniverId() {
        return univerId;
    }

    public void setUniverId(Univer univerId) {
        this.univerId = univerId;
    }
    
    // строковое представление объекта
    @Override
    public String toString() {
        return "Faculty[ facultyId=" + facultyId + " ]";
    }

}
