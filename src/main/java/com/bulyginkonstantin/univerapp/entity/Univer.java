package com.bulyginkonstantin.univerapp.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

/**
 * Класс "Univer" представляет собой таблицу "univer" в базе данных
 */
@Entity
@Table(name = "univer", catalog = "univer_db", schema = "")
public class Univer implements Serializable {

    private static final long serialVersionUID = 3L;
    
    // поле класса представляющее колонку в таблице базы данных univer_id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "univer_id", nullable = false)
    private Integer univerId;
    
    // поле класса представляющее колонку в таблице базы данных univer_name
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "univer_name", nullable = false, length = 45)
    private String univerName;
    
    // поле класса представляющее список факультетов
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "univerId")
    private Collection<Faculty> facultyCollection;

    // конструкторы
    public Univer() {
    }

    public Univer(Integer univerId) {
        this.univerId = univerId;
    }

    public Univer(Integer univerId, String univerName) {
        this.univerId = univerId;
        this.univerName = univerName;
    }

    // гетторы и сетторы
    public Integer getUniverId() {
        return univerId;
    }

    public void setUniverId(Integer univerId) {
        this.univerId = univerId;
    }

    public String getUniverName() {
        return univerName;
    }

    public void setUniverName(String univerName) {
        this.univerName = univerName;
    }

    public Collection<Faculty> getFacultyCollection() {
        return facultyCollection;
    }

    public void setFacultyCollection(Collection<Faculty> facultyCollection) {
        this.facultyCollection = facultyCollection;
    }

    // строковое представление объекта
    @Override
    public String toString() {
        return "Univer[ univerId=" + univerId + " ]";
    }
    
}
