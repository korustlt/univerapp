<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>

        <div class="row">
            <div class="container">
                <h3 class="text-center">Список преподавателей</h3>
                <hr>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Почта</th>
                            <th>Факультет</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach var="teacher" items="${TEACHER_LIST}">

                            <tr>
                                <td>${teacher.firstName}</td>
                                <td>${teacher.lastName}</td>
                                <td>${teacher.email}</td>
                                <td>${teacher.faculty.facultyName}</td>
                            </tr>
                        </c:forEach>
                    </tbody>

                </table>
                <a href="${pageContext.request.contextPath}/" class="navbar-brand">На главную</a>
            </div>
        </div>
    </body>

</html>