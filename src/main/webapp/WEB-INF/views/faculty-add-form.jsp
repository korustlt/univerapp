<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>
        <div class="container col-md-5">
            <div class="card">
                <div class="card-body">
                    <form action="${pageContext.request.contextPath}/addFaculty" method="post">
                        <caption>
                            <h2>
                                Добавить
                            </h2>
                        </caption>

                        <fieldset class="form-group">
                            <label>Название</label>
                            <input type="text" value="<c:out value='${faculty.facultyName}' />" class="form-control" name="facultyName"
                                   required="required">
                        </fieldset>

                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                    <a href="${pageContext.request.contextPath}/editListFaculty" class="navbar-brand">Назад</a>
                </div>
            </div>
        </div>
    </body>

</html>
