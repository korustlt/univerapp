<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>

        <div class="row">
            <div class="container">
                <h3 class="text-center">Список учителей</h3>
                <hr>
                <div class="container text-left">
                    <a href="<%=request.getContextPath()%>/addTeacher" class="btn btn-success">Добавить преподавателя</a>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Почта</th>
                            <th>Факультет</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach var="teacher" items="${TEACHER_LIST}">

                            <tr>
                                <td>${teacher.firstName}</td>
                                <td>${teacher.lastName}</td>
                                <td>${teacher.email}</td>
                                <td>${teacher.faculty.facultyName}</td>
                                <td><a href="${pageContext.request.contextPath}/editTeacher?id=<c:out value='${teacher.teacherId}' />">редактировать</a> | <a href="${pageContext.request.contextPath}/deleteTeacher?id=<c:out value='${teacher.teacherId}' />">удалить</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>

                </table>

                <a href="${pageContext.request.contextPath}/admin" class="navbar-brand">Назад</a>
            </div>
        </div>
    </body>

</html>