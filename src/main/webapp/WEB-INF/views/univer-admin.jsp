<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <h2>Страница редактирования</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Преподаватели</th>
                    <th>Факультеты</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="${pageContext.request.contextPath}/editListTeacher">Редактировать преподавателей</a></td>
                    <td><a href="${pageContext.request.contextPath}/editListFaculty">Редактировать факультеты</a></td>
                </tr>

            </tbody>

        </table>
    </body>
</html>
