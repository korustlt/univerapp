<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true" %>
<html>
    <head>
        <title>Ошибка сервера</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>

        <h1>Произошла ошибка</h1>
        <p><%=exception.getMessage()%></p>
        <a href="${pageContext.request.contextPath}/" class="navbar-brand">
            На главную
        </a>

    </body>
</html>

