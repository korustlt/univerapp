<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>

        <div class="row">
            <div class="container">
                <h3 class="text-center">Список факультетов</h3>
                <hr>
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>Название факультета</th>
                        </tr>
                    </thead>
                    <tbody>

			<c:forEach var="temp" items="${FACULTY_LIST}">
				<tr>
					<td>${temp.facultyName}</td>
				</tr>
			</c:forEach>
                    </tbody>

                </table>
                
                <a href="${pageContext.request.contextPath}/" class="navbar-brand">На главную</a>
            </div>
        </div>
    </body>

</html>