<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>

        <h1>Результат запроса</h1>

        <div class="container col-md-5">


            <c:choose>

                <c:when test="${FACULTY != null}">
                    <table class="table table-bordered">

                        <thead>
                            <tr>
                                <th>Факультет</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${FACULTY.facultyName}</td>
                            </tr>
                        </tbody>
                    </table>
                </c:when>

                <c:otherwise>
                    <table class="table table-bordered">

                        <thead>
                            <tr>
                                <th>Факультет</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Нет данных</td>
                            </tr>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>

            <c:choose>


                <c:when test="${TEACHER_LIST != null}">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Почта</th>
                                <th>Факультет</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach var="teacher" items="${TEACHER_LIST}">

                                <tr>
                                    <td>${teacher.firstName}</td>
                                    <td>${teacher.lastName}</td>
                                    <td>${teacher.email}</td>
                                    <td>${teacher.faculty.facultyName}</td>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>

                <c:otherwise>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Преподаватели</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>нет данных</td>
                            </tr>
                        </tbody>
                    </table>
                </c:otherwise>


            </c:choose>

            <a href="${pageContext.request.contextPath}/search" class="navbar-brand">Назад</a>
        </div>
    </body>
</html>
