<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <h3 class="text-center">Поиск</h3>
        <hr>
        <div class="container col-md-5">
            <div class="card">
                <div class="card-body">
                    <form action="${pageContext.request.contextPath}/search" method="post">

                        <fieldset class="form-group">
                            <label>Поиск факультета</label>
                            <input type="text" class="form-control" name="facultyRequest"
                                   required="required">
                        </fieldset>                                

                        <button type="submit" class="btn btn-success">Искать</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
