<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>
        <div class="container col-md-5">
            <div class="card">
                <div class="card-body">
                    <form action="${pageContext.request.contextPath}/addTeacher" method="post">
                        <caption>
                            <h2>
                                Добавить
                            </h2>
                        </caption>

                        <fieldset class="form-group">
                            <label>Имя</label>
                            <input type="text" value="<c:out value='${teacher.firstName}' />" class="form-control" name="firstName"
                                   required="required">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Фамилия</label>
                            <input type="text" value="<c:out value='${teacher.lastName}' />" class="form-control" name="lastName" required="required">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Почта</label>
                            <input type="text" value="<c:out value='${teacher.email}' />" class="form-control"
                                   name="email" required="required">
                        </fieldset>

                        <fieldset class="form-group">

                            <label>Факультет</label>

                            <select name = "facultyId">

                                <c:forEach var="faculty" items="${FACULTY_LIST}">

                                    <option value="${faculty.facultyId}">${faculty.facultyName}</option>

                                </c:forEach>
                            </select>

                        </fieldset>

                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                    <a href="${pageContext.request.contextPath}/editListTeacher" class="navbar-brand">Назад</a>
                </div>
            </div>
        </div>
    </body>

</html>
