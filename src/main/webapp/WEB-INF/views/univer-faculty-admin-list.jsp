<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>

        <div class="row">
            <div class="container">
                <h3 class="text-center">Список факультетов</h3>
                <hr>
                <div class="container text-left">
                    <a href="<%=request.getContextPath()%>/addFaculty" class="btn btn-success">Добавить факультет</a>
                </div>
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>Название факультета</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach var="faculty" items="${FACULTY_LIST}">
                            <tr>
                                <td>${faculty.facultyName}</td>
                                <td><a href="${pageContext.request.contextPath}/editFaculty?id=<c:out value='${faculty.facultyId}' />">редактировать</a> | <a href="${pageContext.request.contextPath}/deleteFaculty?id=<c:out value='${faculty.facultyId}' />">удалить</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>

                </table>
                <a href="${pageContext.request.contextPath}/admin" class="navbar-brand">Назад</a>
            </div>
        </div>
    </body>

</html>