<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

    <head>
        <title>Хороший университет</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <br>
        <div class="container col-md-5">
            <div class="card">
                <div class="card-body">

                    <form action="${pageContext.request.contextPath}/editTeacher" method="post">

                        <caption>
                            <h2>
                                Редактировать
                            </h2>
                        </caption>

                        <c:if test="${teacher != null}">
                            <input type="hidden" name="teacherId" value="<c:out value='${teacher.teacherId}' />"/>

                        </c:if>

                        <fieldset class="form-group">
                            <label>Имя</label>
                            <input type="text" value="<c:out value='${teacher.firstName}' />" class="form-control" name="firstName"
                                   required="required">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Фамилия</label>
                            <input type="text" value="<c:out value='${teacher.lastName}' />" class="form-control" name="lastName">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Почта</label>
                            <input type="text" value="<c:out value='${teacher.email}' />" class="form-control"
                                   name="email">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Факультет</label>

                            <select name = "facultyId">

                                <c:forEach var="faculty" items="${FACULTY_LIST}">
                                    <c:choose>
                                        <c:when test="${faculty.facultyId == teacher.getFaculty().facultyId}">
                                            <option value="${faculty.facultyId}" selected>${faculty.facultyName}</option>          
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${faculty.facultyId}">${faculty.facultyName}</option> 
                                        </c:otherwise>

                                    </c:choose>

                                </c:forEach>
                            </select>

                            <c:forEach var="faculty" items="${faculties}">


                                <c:choose>
                                    <c:when test="${faculty.facultyId == teacher.getFaculty().facultyId}">
                                        <input type="text" value="<c:out value='${faculty.facultyName}' />" class="form-control"
                                               name="facultyName">
                                        <input type="hidden" name="facultyId" value="<c:out value='${faculty.facultyId}' />"/>
                                    </c:when>

                                </c:choose>

                            </c:forEach>

                        </fieldset>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                    <a href="${pageContext.request.contextPath}/editListTeacher" class="navbar-brand">Назад</a>
                </div>
            </div>
        </div>
    </body>

</html>
